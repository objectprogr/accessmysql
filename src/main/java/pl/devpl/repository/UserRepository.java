package pl.devpl.repository;

import org.springframework.data.repository.CrudRepository;

import pl.devpl.model.User;

public interface UserRepository extends CrudRepository<User, Long>{

	User findById(Integer id);
	User findByUsername(String username);


}
