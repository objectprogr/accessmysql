package pl.devpl.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.devpl.model.User;
import pl.devpl.repository.UserRepository;

@org.springframework.stereotype.Controller
public class Controller {

	@Autowired
	private UserRepository repository;
	Logger log = LoggerFactory.getLogger(this.getClass());

	@GetMapping(path="/all")
	public String display(Model model) {
		model.addAttribute("now", repository.findAll());
		return "index";
	}
	@RequestMapping(value="/")
	public String home(Model model) {
		return "home";
	}
	
	@GetMapping(path="/form")
	public String userForm(Model model) {
		model.addAttribute("user", new User());
		return "form";
	}
	@RequestMapping(value="/form", method=RequestMethod.POST)
	public String userSubmit(@ModelAttribute User user, Model model) {
		model.addAttribute("user", user);
		String info = String.format("User Submission: id = %d, username = %s, password = %s", 
				user.getId(), user.getUsername(), user.getPassword());
		log.info(info);
		repository.save(user);

		return "result";
	}
	
	@GetMapping(path="/find")
	public String findUser(Model model) {
		model.addAttribute("user", new User());
		return "find";
	}

    @RequestMapping(value="/find", method=RequestMethod.POST)
    public String customerSubmit(@RequestParam("username") String username,
    		@RequestParam("id") Integer id, Model model) {
//    	if(username != null) {
//    		User user = repository.findByUsername(username);
//    		model.addAttribute("user", user);
//    	}else if(id != 0) {
//    		User user = repository.findById(id);
//    		model.addAttribute("user", user);
//    	}
    	
    	User user = repository.findByUsername(username);
    	model.addAttribute("user", user);
        return "load";
    }
    
	@GetMapping(path="/update")
	public String updateForm(Model model) {
		model.addAttribute("user", new User());
		return "update";
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public String update(@RequestParam("id") Integer id, @ModelAttribute User user ,Model model) {
		model.addAttribute("user", user);
		User userToUpdate = new User();
		userToUpdate = repository.findById(id);
		userToUpdate.setUsername(user.getUsername());
		userToUpdate.setPassword(user.getPassword());
		repository.save(userToUpdate);
		return "updateResult";
		
	}

}
